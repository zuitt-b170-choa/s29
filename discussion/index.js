// this require ("express") allows devs to load/import express package that will be used for the application.
let express = require('express');

// express() - allows devs to create an application using express
const app = express(); // this code creates an express application and stores it inside the "app" variable, thus, "app" is the server already.

const port = 3000;

let users = [
{
	"username": "johndoe",
	"password": "johndoe1234"
}];


// use function lets the middleware to do common services and capabilities to the applications
app.use(express.json()); // lets the app to read json data
app.use(express.urlencoded({extended: true})); // allows the app to receive data from the forms. 
// not using these will result to our req.body to return undefined

//Express has methods corresponding to each http methods (get, post, put, delete, etc.)
//get method is used to specify for the "/" url, with req and res paramters that will be sent to our arrow function.
// this "/" route expects to receive a get request at its endpoint (http://localhost:3000/)
app.get("/", (req,res)=>{
	//res.send - allows sending of messages as responses to the client
	res.send("Hello World")
})


app.get("/hello", (req,res)=>{
	res.send("hello from the /hello end")
})

//this "/hello" route is expected to receive a post request that json data in the body
app.post("/hello", (req,res)=>{
	// to check if the json keys in the body have values.
	console.log(req.body.firstName)
	console.log(req.body.lastName)
	// sends the response once the req.body.firstName and req.body.lastName have values.
	res.send(`hello there ${req.body.firstName} ${req.body.lastName}`)
})

// activity
app.get("/home", (req,res)=>{
	res.send(`Welcome to the home page.`)
})

app.get("/users", (req,res)=>{
	res.send(users);
})

app.delete("/delete-user", (req,res)=>{

	let message;
	if(users.length != 0){
		for(let i=0; i<users.length; i++){
		if(req.body.username == users[i].username){
			users.splice(users[i],1);
			message = `User ${req.body.username} has been deleted.`;
			break;
		}
	}
	}else if(req.body.username !== users[i].username){
			message = "User does not exist";
	}
	res.send(message);
	console.log(users);
})

app.post("/signup", (req,res)=>{
	if(req.body.username != "" || req.body.password != ""){
	users.push(req.body);
	

	res.send(`User ${req.body.username} successfully registered.`);
	console.log(users);
	}
	else{
		res.send(`Incorrect Input.`)
	}
})

// create a sign up route that will accept a post method that will check not equal to null req.body.password
app.listen(port, ()=>console.log(`Server is running at port ${port}`));
